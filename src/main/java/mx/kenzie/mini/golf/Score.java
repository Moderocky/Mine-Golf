package mx.kenzie.mini.golf;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import mx.kenzie.mini.logic.ASerialisable;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.UUID;

public class Score extends ASerialisable implements Comparable<Score> {

    public final String name;
    public final UUID uuid;
    public final byte score;

    public Score(JsonElement element) {
        super();
        JsonObject object = element.getAsJsonObject();
        name = object.get("name").getAsString();
        uuid = UUID.fromString(object.get("uuid").getAsString());
        score = object.get("score").getAsByte();
    }

    public Score(String name, UUID uuid, int score) {
        this.name = name;
        this.uuid = uuid;
        this.score = (byte) score;
    }

    public boolean samePerson(Score score) {
        return uuid.equals(score.uuid);
    }

    public int getScore() {
        return score;
    }

    @Override
    public int compareTo(@NotNull Score o) {
        return Byte.compare(score, o.score);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Score)) return false;
        Score score1 = (Score) o;
        return score == score1.score &&
                Objects.equals(uuid, score1.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid, score);
    }

    @Override
    protected boolean isClassloaded() {
        return false;
    }

    @Override
    protected void load(JsonObject object) {
    }

    @Override
    public JsonObject save() {
        JsonObject object = new JsonObject();
        object.addProperty("name", name);
        object.addProperty("uuid", uuid.toString());
        object.addProperty("score", score);
        return object;
    }
}
