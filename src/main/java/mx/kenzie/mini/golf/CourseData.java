package mx.kenzie.mini.golf;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import mx.kenzie.mini.logic.ASerialisable;
import mx.kenzie.mini.logic.Utils;
import org.bukkit.Location;
import org.bukkit.block.Block;

public class CourseData extends ASerialisable {
    public Block start;
    public Block finish;
    public byte difficulty;
    public byte par;

    public CourseData(JsonElement element) {
        super(element);
    }

    public CourseData(Location start, Location finish, int difficulty, int par) {
        super();
        this.start = start.getBlock();
        this.finish = finish.getBlock();
        this.difficulty = (byte) difficulty;
        this.par = (byte) par;
    }

    @Override
    protected boolean isClassloaded() {
        return false;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public int getPar() {
        return par;
    }

    @Override
    protected void load(JsonObject object) {
        start = Utils.INSTANCE.from(object.get("start")).getBlock();
        finish = Utils.INSTANCE.from(object.get("finish")).getBlock();
        difficulty = object.get("difficulty").getAsByte();
        par = object.get("par").getAsByte();
    }

    @Override
    public JsonObject save() {
        JsonObject object = new JsonObject();
        object.add("start", Utils.INSTANCE.from(start.getLocation()));
        object.add("finish", Utils.INSTANCE.from(finish.getLocation()));
        object.addProperty("difficulty", difficulty);
        object.addProperty("par", par);
        return object;
    }
}
