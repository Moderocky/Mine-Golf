package mx.kenzie.mini.golf;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.moderocky.mask.api.MagicList;
import mx.kenzie.mini.MiniGolf;
import mx.kenzie.mini.logic.ASerialisable;
import org.bukkit.NamespacedKey;

import java.util.Collections;
import java.util.UUID;

public class Course extends ASerialisable {

    private final MagicList<Score> scores = new MagicList<>();
    private CourseData data;
    private NamespacedKey key;
    private String name;


    public Course(JsonObject data) {
        super(data);
    }

    public Course(String id, String name, CourseData courseData) {
        super();
        this.key = MiniGolf.getNamespacedKey(id);
        this.name = name;
        this.data = courseData;
    }

    // Returns -1 if not a high-score.
    public int registerScore(Score score) {
        scores.add(score);
        Collections.sort(scores);
        Collections.reverse(scores);
        while (scores.size() > 10) scores.remove(10);
        return scores.indexOf(score);
    }

    public boolean hasHighscore(UUID uuid) {
        return scores.oneMatches(score -> score.uuid == uuid);
    }

    public MagicList<Score> getHighScores() {
        return new MagicList<>(scores);
    }

    @Override
    protected boolean isClassloaded() {
        return false;
    }

    @Override
    protected void load(JsonObject object) {
        data = new CourseData(object.get("course_data"));
        key = MiniGolf.getNamespacedKey(object.get("id").getAsString());
        name = object.get("name").getAsString();
        for (JsonElement element : object.getAsJsonArray("scores")) {
            scores.add(new Score(element));
        }
    }

    @Override
    public JsonObject save() {
        JsonObject object = new JsonObject();
        object.add("course_data", data.save());
        object.addProperty("id", key.getKey());
        object.addProperty("name", name);
        object.add("scores", scores.toJsonArray(Score::save));
        return object;
    }

}
