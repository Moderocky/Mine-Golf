package mx.kenzie.mini.logic;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import java.util.UUID;

public class Utils {

    public static final Utils INSTANCE = new Utils();

    public Location from(JsonElement element) {
        if (!(element instanceof JsonObject)) return null;
        JsonObject object = element.getAsJsonObject();
        World world;
        try {
            world = Bukkit.getWorld(UUID.fromString(object.get("world").getAsString()));
        } catch (Throwable throwable) {
            world = Bukkit.getWorld(object.get("world").getAsString());
        }
        double x = object.get("x").getAsDouble();
        double y = object.get("y").getAsDouble();
        double z = object.get("z").getAsDouble();
        return new Location(world, x, y, z);
    }

    public JsonObject from(Location location) {
        JsonObject object = new JsonObject();
        object.addProperty("world", location.getWorld().getName());
        object.addProperty("x", location.getX());
        object.addProperty("y", location.getY());
        object.addProperty("z", location.getZ());
        return object;
    }

}
