package mx.kenzie.mini.logic;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public abstract class ASerialisable {

    public ASerialisable() {
    }

    public ASerialisable(JsonElement element) {
        if (!(element instanceof JsonObject)) throw new IllegalArgumentException();
        load(element.getAsJsonObject());
    }

    protected abstract boolean isClassloaded();

    protected abstract void load(JsonObject object);

    public abstract JsonObject save();

}
